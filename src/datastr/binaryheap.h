#pragma once

#include <memory>
#include <cmath>
#include <optional>
#include <stdexcept>
#include <type_traits>

namespace ads { namespace types {

/**
 * @brief           Data structure that performs 4 operations: insert, decreaseKey, getMin, extractMin
 * @tparam T        Comparable type
 * @tparam Alloc    Allocator type
 * @tparam Compare  Comparator functional object
 */
template <class T, class Alloc = std::allocator<T>, class Compare = std::less<T>>
class BinaryHeap {

    [[no_unique_address]] Alloc _alloc;
    [[no_unique_address]] Compare _comp;
    T* _tree;
    size_t _cap;
    size_t _sz;

public:

    constexpr BinaryHeap() noexcept : _tree(nullptr) {}

    constexpr BinaryHeap(Alloc alloc) noexcept : _alloc(alloc), _comp(), _tree(nullptr) {}

    constexpr BinaryHeap(Alloc alloc, Compare comp) noexcept : _alloc(alloc), _comp(comp), _tree(nullptr) {}

    constexpr BinaryHeap(Compare comp) noexcept : _alloc(), _comp(comp), _tree(nullptr) {}

    BinaryHeap(size_t n) : _alloc(), _comp(), _tree(std::allocator_traits<Alloc>::allocate(_alloc, n)), _cap(n), _sz(0ul) {}

    BinaryHeap(size_t n, Alloc alloc) : _alloc(alloc), _comp(), _tree(std::allocator_traits<Alloc>::allocate(_alloc, n)), _cap(n), _sz(0ul) {}

    BinaryHeap(size_t n, Alloc alloc, Compare comp) : _alloc(alloc), _comp(comp), _tree(std::allocator_traits<Alloc>::allocate(_alloc, n)), _cap(n), _sz(0ul) {}

    BinaryHeap(size_t n, Compare comp) : _alloc(), _comp(comp), _tree(std::allocator_traits<Alloc>::allocate(_alloc, n)), _cap(n), _sz(0ul) {}

    ~BinaryHeap() noexcept {
        for (auto i = 0ul; i < _sz; ++i) {
            std::allocator_traits<Alloc>::destroy(_alloc, _tree + i);
        }
        std::allocator_traits<Alloc>::deallocate(_alloc, _tree, _cap);
    }

    BinaryHeap(const BinaryHeap& other) : _alloc(other._alloc), _comp(other._comp) {
        // TODO: copy ctor (how should manage memory?)
    }

    /**
     * @brief   Returns the root of the tree, which is actually a minimum
     * @return  std::optional<T> object which will be empty if the heap is empty
     */
    std::optional<T> getMin() {
        return _sz != 0ul ? std::optional<T>(*_tree) 
                          : std::optional<T>();
    }

    /**
     * @brief       Inserts new key into the correct place in the heap
     * @tparam U    Same as T, universal reference friendly
     * @param item  Inserting item
     */
    template <class U>
    void insert(U&& item) {
        if (_sz == _cap)
            reserve();

        _tree[_sz++] = std::forward<U>(item);
        siftUp(_sz - 1ul);
    }

    /**
     * @brief   Removes root (minimum) element
     */
    void extractMin() {
        if (_sz == 0)
            return;

        if (_sz == 1) {
            std::allocator_traits<Alloc>::destroy(_alloc, _tree);
            --_sz;
        }

        *_tree = std::move(_tree[--_sz]);
        std::allocator_traits<Alloc>::destroy(_alloc, _tree + _sz);
        siftDown(0ul);
    }

    /**
     * @brief       Decreases key in the heap with index ind
     * @param ind   Index of the key
     * @param delta Subtrahend
     */
    void decreaseKey(size_t ind, const T& delta) {
        if (ind >= _sz || ind < 0ul)
            throw std::out_of_range("Trying to access non-existing key in the heap");

        _tree[ind] -= delta;
        siftUp(ind);
    }

    /**
     * @brief       Decreases key in the heap pointed by ptr
     * @param ptr   Pointer to the key
     * @param delta Subtrahend
     */
    void decreaseKey(T* ptr, const T& delta) {
        if (ptr - _tree >= _sz || ptr - _tree < 0ul)
            throw std::out_of_range("Trying to access non-existing key in the heap");

        *ptr -= delta;
        siftUp(_tree[ptr - _tree]);
    }

    template <class Iter>
    static void make_heap(Iter begin, Iter end, 
            Compare comp = std::less<std::remove_reference_t<decltype(std::declval<Iter>().operator*())>>()) {
        int _baseind = 0, _size = end - begin;
        while (begin != end) {
            int _ind = _baseind;
            while (2 * _ind + 2 <= _size) {
                int _next = 2 * _ind + 1;

                if (_next + 1 <= _size && comp(*(begin + _next + 1), *(begin + _next))) 
                    ++_next;

                if (comp(*(begin + _next), *(begin + _ind))) {
                    std::iter_swap(begin + _next, begin + _ind);
                    _ind = _next;
                } else 
                    break;
            }
            ++begin;
            ++_baseind;
        }
    }

private:

    /**
     * @brief       Sets changed key higher in the tree
     * @param _ind  Index of the key
     */
    void siftUp(size_t _ind) {
        while (_ind != 0) {
            if (_comp(_tree[_ind], _tree[(_ind - 1ul) / 2ul])) {
                std::swap(_tree[_ind], _tree[(_ind - 1ul) / 2ul]);
                _ind = (_ind - 1ul) / 2ul;
            } else 
                break;
        }
    }

    /**
     * @brief       Sets changed key lower in the tree
     * @param _ind  Index of the key
     */
    void siftDown(size_t _ind) {
        while (2 * _ind + 2ul <= _sz) {
            size_t _next = 2 * _ind + 1ul;

            if (_next + 1ul <= _sz && _comp(_tree[_next + 1ul], _tree[_next])) {
                _next += 1ul;
            }

            if (_comp(_tree[_next], _tree[_ind])) {
                std::swap(_tree[_next], _tree[_ind]);
                _ind = _next;
            } else 
                break;
        }
    }

    /**
     * @brief If there is no any space left, allocates more memory and copies/moves old data to new place
     */
    void reserve() {
        auto cap = std::max(1ul, 2 * _cap);
        auto _copy = std::allocator_traits<Alloc>::allocate(_alloc, cap);

        if (_cap != 0ul) {
            for (size_t i = 0ul; i < _sz; ++i) {
                std::allocator_traits<Alloc>::construct(_alloc, _copy + i, std::move(_tree[i]));
                std::allocator_traits<Alloc>::destroy(_alloc, _tree + i);
            }
            std::allocator_traits<Alloc>::deallocate(_alloc, _tree, _cap);
        }

        _cap = cap;
        _tree = _copy;
    }
};

} // <-- namespace types
} // <-- namespace ads