#pragma once

#include <memory>
#include <optional>
#include <stdexcept>

namespace ads {

template <class T, class Alloc = std::allocator<T>>
class Stack {

    [[no_unique_address]] Alloc _alloc;
    T* _base;
    T* _curr;
    T* _endbuf;

public:

    /**
     * @brief   Stack destructor
     */
    ~Stack() noexcept {
        T*__base = _base;
        while(__base != _curr) {
            std::allocator_traits<Alloc>::destroy(_alloc, __base++);
        }

        std::allocator_traits<Alloc>::deallocate(_alloc, _base, _endbuf - _base);
    }

    /**
     * @brief Default Stack ctor
     */
    constexpr Stack() noexcept : _alloc(), _base(nullptr), _curr(nullptr), _endbuf(nullptr) {}

    /**
     * @brief   Creates Stack object for n allocated objects of type T
     * @param n Amount of allocated objects of type T
     */
    Stack(size_t n) : _alloc(),
                      _base(std::allocator_traits<Alloc>::allocate(_alloc, n)),
                      _curr(_base), 
                      _endbuf(_base + n) {}

    /**
     * @brief       Stack copy ctor
     * @param other Another Stack object
     */
    Stack(const Stack &other) :
        _alloc(other._alloc), 
        _base(std::allocator_traits<Alloc>::allocate(_alloc, other._endbuf - other._base)),
        _curr(_base + (other._curr - other._base)), _endbuf(_base + other._buf - other._base)
    {
        T* _begin = other._base;
        T* __base = _base;
        while (_begin != other._curr) {
            std::allocator_traits<Alloc>::construct(_alloc, __base++, *_begin++);
        }
    }

    /**
     * @brief       Stack move ctor
     * @param other Rvalue Stack object
     */
    Stack(Stack&& other) :
        _alloc(std::move(other._alloc)),
        _base(std::allocator_traits<Alloc>::allocate(_alloc, other._endbuf - other._base)),
        _curr(_base + (other._curr - other._base)), _endbuf(_base + (other._endbuf - other._base))
    {
        T* _begin = other._base;
        T* __base = _base;
        while (_begin != other._curr) {
            std::allocator_traits<Alloc>::construct(_alloc, __base++, std::move(*_begin++));
        }
    }

    /**
     * @brief   Returns true if the stack is empty
     */
    bool empty() const noexcept { return _base == _curr; }

    /**
     * @brief   Returns element from the top of the stack
     */
    const std::optional<T&> front() const {
        return (_curr != _base) ? std::optional<T&>(*(_curr - 1)) : std::optional<T&>();
    }

    /**
     * @brief Takes and returns the element on the top of the stack
     */
    std::optional<T> pop() {
        if (_curr == _base) 
            return std::optional<T>();

        std::optional<T> popped(std::move(*(_curr - 1)));
        std::allocator_traits<Alloc>::destroy(_alloc, _curr-- - 1);
        return popped;
    }

    /**
     * @brief       Pushes element to the stack
     * @tparam U    U == T (see universal references doc)
     * @param obj   Pushing object
     */
    template <class U>
    void push(U&& obj) {
        if (_curr == _endbuf)
            reserve();

        *_curr++ = std::forward<U>(obj);
    }

private:

    /**
     * @brief If there is no any space left, allocates more memory and copies/moves old data to _copy
     */
    void reserve() {
        auto cap = std::max(1l, 2 * (_endbuf - _base));
        auto _copy = std::allocator_traits<Alloc>::allocate(_alloc, cap);

        if (_endbuf != _base) {
            auto _it = _base;
            auto _itcopy = _copy;
            while (_it != _curr) {
                std::allocator_traits<Alloc>::construct(_alloc, _itcopy++, std::move(*_it));
                std::allocator_traits<Alloc>::destroy(_alloc, _it++);
            }
            std::allocator_traits<Alloc>::deallocate(_alloc, _base, _endbuf - _base);
        }
        
        _curr = _copy + (_curr - _base);
        _base = _copy;
        _endbuf = _copy + cap;
    }

};

} // <-- namespace ads