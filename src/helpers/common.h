#pragma once

#include <iostream>
#include <algorithm>
#include <iterator>
#include <random>
#include <vector>
#include <experimental/random>

namespace helpers {

/**
 * @brief       Prints the input array [begin, end] (set as two pointers)
 * @tparam T    Template parameter of the array
 * @param begin Pointer to the begin of the input array
 * @param end   Pointer to the end of the input array
 * @param msg   [optional] In-place constructed string object which is printed before the array
 * @return      Nothing
 * @example     print(begin, end, "My input array: ")
 */
template <class T>
void print(T* begin, T* end, std::string msg = std::string{}) {
    std::cout << msg;
    std::copy(begin, end + 1, std::ostream_iterator<T>(std::cout, " "));
    std::cout << '\n';
} 

/**
 * @brief       Print overload for iterators
 * @tparam Iter Iterator template type
 * @param begin Iterator to the begin of input array
 * @param end   Iterator to the end of input array
 * @param msg   [optional] In-place constructed string object which is printed before the array
 * @return      Nothing
 * @example     print(vec.begin(), vec.end(), "My input array: ")
 */
template <class Iter>
void print(Iter begin, Iter end, std::string msg = std::string{}) {
    std::cout << msg;
    std::copy(begin, end, std::ostream_iterator<decltype(std::declval<Iter>().operator*())>(std::cout, " "));
    std::cout <<'\n';
}

template <class T>
std::vector<T> randVector(T lowerBound, T upperBound, unsigned long size = 10) {
    std::vector<T> tvec(size);
    for (auto& item : tvec) {
        item = std::experimental::randint(lowerBound, upperBound);
    }
    return tvec;
}

} // <-- namespace helpers