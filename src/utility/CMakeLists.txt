set(utilitylib utility)

file(GLOB_RECURSE UTILITY_SRC
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/*.h
        )

add_library(${utilitylib} SHARED ${UTILITY_SRC})
set_target_properties(${utilitylib} PROPERTIES
        IMPORTED_LOCATION "${PROJECT_SOURCE_DIR}/lib/libutility.so"
        INCLUDE_DIRECTORIES "${PROJECT_SOURCE_DIR}/include/${utilitylib}"
        LINKER_LANGUAGE CXX
        )
