#pragma once

#include <experimental/random>
#include <iostream>

namespace ads { namespace sort {

/**
 * @brief       Partition function of QuickSort algorithm
 * @param arr   Input array
 * @param begin Index where array begins 
 * @param end   Index where array ends
 * @param comp  Comparator function
 * @return      Index of the pivot
 */
template <class T, class Compare>
unsigned long partition(T* arr, unsigned long first, unsigned long last, Compare comp) {
    auto pivot = std::experimental::randint(first, last);
    
    while (first != pivot) {
        if (comp(arr[first], arr[pivot])) {
            ++first;
        } else {
            std::swap(arr[first], arr[pivot - 1]);
            std::swap(arr[pivot], arr[pivot - 1]);
            --pivot;
        }
    }

    while (last != pivot) {
        if (comp(arr[pivot], arr[last])) {
            --last;
        } else {
            std::swap(arr[last], arr[pivot + 1]);
            std::swap(arr[pivot], arr[pivot + 1]);
            ++pivot;
        }
    }

    return pivot;
}

/**
 * @brief        Quicksort algorithm
 * @param  arr   Input array
 * @param  begin Index where array begins (can be not 0)
 * @param  end   Index where array ends
 * @param  comp  Comparator function
 * @return       Nothing
*/
template <class T, class Compare>
void quicksort(T* arr, unsigned long first, unsigned long last, Compare comp) {
    if (first >= last)
        return;

    if (last - first == 1) {
        if (comp(arr[last], arr[first])) 
            std::swap(arr[last], arr[first]);
        return;
    }

    auto pivot = partition(arr, first, last, comp);
    quicksort(arr, first, pivot, comp);
    quicksort(arr, pivot, last, comp);
}

} // <-- namespace sort
} // <-- namespace ads