#pragma once

#include <algorithm>

namespace ads { namespace sort {

template <class T, class Compare>
void merge(T* bufleft, unsigned long sizeleft, T* bufright, unsigned long sizeright, T* dest, Compare comp) {

    int i = 0, j = 0;

    while (i != sizeleft) {

        if (j == sizeright)  {
            std::copy(bufleft + i, bufleft + sizeleft, dest);
            return;
        }

        *dest = comp(bufleft[i], bufright[j]) ? bufleft[i++] : bufright[j++];
        dest++;
    }

    std::copy(bufright + j, bufright + sizeright, dest);
}

template <class T, class Compare>
void mergesort(T* arr, unsigned long size, Compare comp) {
    if (size == 1)
        return;

    if (size == 2) {
        if (comp(arr[1], arr[0]))
            std::swap(arr[1], arr[0]);
        return;
    }

    // <-- heap allocation in case of sizeof(T) * size >= stack free memory
    T* bufleft = new T[size / 2];
    std::copy(arr, arr + size / 2, bufleft);
    T* bufright = new T[size - size / 2];    
    std::copy(arr + size / 2, arr + size, bufright);

    mergesort(bufleft, size / 2ul, comp);
    mergesort(bufright, size - size / 2ul, comp);

    merge(bufleft, size / 2ul, bufright, size - size / 2ul, arr, comp);

    delete[] bufleft;
    delete[] bufright;
}

} // <-- namespace sort
} // <-- namespace ads