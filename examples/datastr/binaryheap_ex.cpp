#include <helpers/common.h>
#include <datastr/binaryheap>

#include <iostream>
#include <experimental/random>

int main() {

    ads::types::BinaryHeap<int> heap;

    for (int i = 0; i < 1; ++i) {

        try {

            int action = std::experimental::randint(1, 6);

            switch (action) {
                case 1: {
                            auto min = heap.getMin();
                            std::cout << "getMin(): " << (min ? std::to_string(min.value()) : "---") << '\n';
                            break;
                        }
                case 2: {
                            auto sub = std::experimental::randint(0, 10);
                            heap.decreaseKey(sub, sub);
                            std::cout << "decreaseKey(" << sub << ", " << sub << ")" << '\n';
                            break;
                        }
                case 3: heap.extractMin();
                        std::cout << "extractMin()" << '\n';
                        break;
                default:    heap.insert(i % 100);
                            std::cout << "insert(" << i % 100 << ")" << '\n';
                            break;
            }
        } catch(std::exception& ex) {
            std::cerr << "Failed to decrease key: " << ex.what() << std::endl;
        }
    }

    for (int i = 0; i < 10; ++i) {
        auto vec = helpers::randVector(0, 100, 7);
        helpers::print(vec.begin(), vec.end(), "Vec before: ");
        ads::types::BinaryHeap<int>::make_heap(vec.begin(), vec.end());
        helpers::print(vec.begin(), vec.end(), "Heapified vec: ");
    }
}