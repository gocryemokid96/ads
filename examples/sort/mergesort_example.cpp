#include <sort/mergesort.h>

#include <random>
#include <vector>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <functional>
#include <cassert>

template <class T>
void print(const std::string& msg, const std::vector<T>& vec) {
    std::cout << msg;
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<> dis(0, 9);
 
    std::vector<int> v1(10), v2(11);
    std::generate(v1.begin(), v1.end(), std::bind(dis, std::ref(mt)));
    std::generate(v2.begin(), v2.end(), std::bind(dis, std::ref(mt)));

    print("Vector v1 before: ", v1);
    print("Vector v2 before: ", v2);

    ads::sort::mergesort(v1.data(), v1.size(), std::less<decltype(v1)::value_type>());
    ads::sort::mergesort(v2.data(), v2.size(), std::less<decltype(v2)::value_type>());

    print("Vector v1 after: ", v1);
    print("Vector v2 after: ", v2);

    assert((void("Critical: not sorted!"), std::is_sorted(v1.begin(), v1.end()) && std::is_sorted(v2.begin(), v2.end())));
}