#include <sort/quicksort.h>
#include <helpers/common.h>

#include <algorithm>
#include <vector>
#include <iterator>
#include <iostream>

int main() {

    for (int i = 0; i < 10; ++i) {
        auto ivec = helpers::randVector<int>(1, 2, 5); 
        helpers::print(ivec.begin(), ivec.end(), "Start algo: ");

        ads::sort::quicksort(ivec.data(), 0ul, ivec.size() - 1, std::less<int>());

        if (!std::is_sorted(ivec.begin(), ivec.end())) {
            std::cout << "Critical: vector is not sorted!" << std::endl;
            return 0;
        } else {
            std::cout << "Sorted" << std::endl;
        }
    }

}